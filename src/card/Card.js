import React from "react";

const Card = props => {
    const classSuit = () => {
        if (props.suit === 's') {
            return 'spades'
        }
        if (props.suit === 'd') {
            return 'diams'
        }
        if (props.suit === 'c') {
            return 'clubs'
        }
        if (props.suit === 'h') {
            return 'hearts'
        }
    };
    const suitCode = () => {
        if (props.code === 's') {
            return '&#9824;'
        }
        if (props.code === 'd') {
            return '&#9830;'
        }
        if (props.code === 'c') {
            return '&#9827;'
        }
        if (props.code === 'h') {
            return '&#9829;'
        }
    }

    return (
        <div className="playingCards faceImages">
            <div className={`card rank-${props.rank.toLowerCase()} ${classSuit()}`}>
                <span className="rank">{props.rank}</span>
                <span className="suit">{suitCode()}</span>
            </div>

        </div>
    );
};

export default Card;