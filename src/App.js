import React, {Component} from 'react';
import './App.css';
import './assets/cards.css';
import Card from "./card/Card";
import Hand from "./Hand";


const SUITS = ["d", "c", "h", "s"];
const RANKS = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];

class App extends Component {
    constructor(props) {
        super(props);
        const deck = [];
        for (let suit of SUITS) {
            for (let rank of RANKS) {
                deck.push({suit: suit, rank: rank})
            }
        }
        this.state = {
            fullDeck: deck,
            fiveCards: []
        }
    }

    getCard = () => {
        const deck = [...this.state.fullDeck];
        let cardIndex = Math.floor(Math.random() * deck.length)
        let card = deck.splice(cardIndex, 1)[0]
        this.setState({fullDeck: deck})
        return card;
    }

    getCards = (howMany) => {
        const fiveCards = [];
        for (let i = 0; i < howMany; i++) {
            const oneCard = this.getCard();
            fiveCards.push(oneCard);
        }
        this.setState({fiveCards: fiveCards})
    }

    render() {
        let cards = []
        if (this.state.fiveCards) {
            cards = this.state.fiveCards.map(card => {
                return <Card suit={card.suit} rank={card.rank} key={this.state.fiveCards.indexOf(card)}/>
            })
        }
        return (
            <div className="App">
                {cards}
                <Hand
                    fiveCards={this.state.fiveCards}
                />
                <div>
                    <button onClick={() => this.getCards(5)}>Render</button>
                </div>
            </div>
        );
    }
}

export default App;