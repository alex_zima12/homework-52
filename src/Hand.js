import React, {Component} from 'react';

class Hand extends Component {
    getOutcome = (arr) => {
        const result = [];

        arr.forEach(card => {
            if (!result.includes(card.rank)) {
                result.push(card.rank)
            }
        });

        switch (result.length) {
            case 4:
                return "One pair"
            default:
                return 'No matches found'
        }
    }

    render() {

        return (
            <div>
                {this.getOutcome(this.props.fiveCards)}
            </div>
        );
    }
}

export default Hand;